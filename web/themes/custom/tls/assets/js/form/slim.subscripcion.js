var correoSubs = document.querySelector('#correo')
var terCondicionesSubs = document.querySelector('#terCondiciones')
var tls_formSubs = document.querySelector('#subs_tls_form')

let fields = document.querySelectorAll('#subs_tls_form input');
let recorreValidar = (fields ) => {
    for (let j = 0; j < fields.length; j++) {
        //Ejecución por cada field
        fields[j].addEventListener('input', function ({ target }) {
            validar(target);
        })    
    }
}
//Llamando a la validacion de campo en campo
recorreValidar(fields);

tls_formSubs.addEventListener('submit', function (e) {
    e.preventDefault()
    let comodin = []
    for (let j = 0; j < fields.length; j++) {
          //Validacion total 
          validar(fields[j])
          if(fields[j].parentNode.classList.contains('warning')){
              comodin.push(fields[j])
          }
      }
    if (comodin.length === 0){
        //Function get UTM
        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        var utm_source = getParameterByName('utm_source')
        var utm_medium = getParameterByName('utm_medium')
        var utm_campaign = getParameterByName('utm_campaign')
        var utm_content = getParameterByName('utm_content')
        var utm_term = getParameterByName('utm_term')
        var fbclid = getParameterByName('fbclid')

        if (terCondicionesSubs.checked){
            var acepto = 'SI'
        }
        let AceptaPublicidad;
        var subs_terPromos = document.querySelector('#terPromos').checked
        switch (subs_terPromos) {
            case true:
                AceptaPublicidad = 'SI'
                break;
            case false:
                AceptaPublicidad = 'NO'
                break;
        }
        var formData = {
            "Email": correoSubs.value,
            "AceptacionTerminos": acepto,
            "utm_source": utm_source,
            "utm_medium": utm_medium,
            "utm_campaign": utm_campaign,
            "utm_content": utm_content,
            "utm_term": utm_term,
            "fbclid": fbclid,
            // 'Indicador': indicador,
            'url_source': tls_formSubs.getAttribute('id'),
            'AceptaPublicidad':AceptaPublicidad

        }
        //console.log(formData)

        var service = '/api/post-leads-crm' // For nginx
        var service_persistent = '/api/post-leads' // For nginx

        //var service ='/apiRest/public/api/post-leads' //For Apache
        fetch(service, {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(data => {
                //console.log(data);
            })
            .then(document.querySelector('#enviando').disabled = true)
            .then(window.location.href = "/gracias")
            .catch(function (error) {
                console.log('Hubo un problema con la petición Fetch:' + error.message);
            })

        fetch(service_persistent, {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            }
            }).then(res => res.json())
                .then(data => {
                    //console.log(data);
                })
                .then(document.querySelector('#enviando').disabled = true)
                
                .catch(function (error) {
                    console.log('Hubo un problema con la petición Fetch:' + error.message);
                })

    }

})