let regexABC = /^[a-zA-ZÀ-ÿ\u00f1\u00d1\s]+$/gi;
let space = /^[\s]+$/gi;
let regexMail = /\b[\w]+@[\w-]+(?:\.[\w]+)+\b/;
let validar = (target) => {
    switch (target.type) {
        case 'text':
            if (target.value.match(regexABC) === null) {
                onlyText = target.value.replace(/[^a-zA-ZÀ-ÿ\u00f1\u00d1\s]/gi, '');
                target.value = onlyText;
            }
            //No permite poner puros espacios en el input
            if (target.value.match(space)) {
                onlyText = target.value.replace(/[\s]/gi, '');
                target.value = onlyText;
            }
            //Validar campos con caracteres
            if (target.value.length === 0 ) {
                target.parentNode.classList.add('warning');
            } else {
                target.parentNode.classList.remove('warning')
            }
            break;
        case 'tel':
            var onlyNumbers;
            //Elimina caracteres especiales letras y espacios  
            if (target.value.match(/[a-zA-ZÀ-ÿ\u00f1\u00d1\W\s]/gi)) {
                onlyNumbers = target.value.replace(/[a-zA-ZÀ-ÿ\u00f1\u00d1\W\s]/gi, '');
                target.value = onlyNumbers;
            }
            if (target.value.length < target.maxLength) {
                target.parentNode.classList.add('warning');
            } else {
                target.parentNode.classList.remove('warning');
            }
            break;
        case 'email':
            var onlyEmail;
            //correo correcto
            if (target.value.match(regexMail)) {
              target.parentNode.classList.remove('warning');
            }
            //correo incorrecto
            if (target.value.match(regexMail) === null) {
              target.parentNode.classList.add('warning');
            }
            break;
        case 'select-one':
            if(target.value.trim().length === 0){
                target.parentNode.classList.add('warning');
            }else{
                target.parentNode.classList.remove('warning');
            }

            break;
        /*case 'checkbox':
                if (target.checked === false ) {
                    target.parentNode.classList.add('warning');
                }else{
                    target.parentNode.classList.remove('warning');
                }
                break;*/
    } 
    switch (target.name) {
        case 'terCondiciones':
        if (target.checked === false ) {
                    target.parentNode.classList.add('warning');
                }else{
                    target.parentNode.classList.remove('warning');
                }
                break;
    }
    
}

let geTCookie =  (name) => {
    let cookieArr = document.cookie.split(";");
    for(let i = 0; i < cookieArr.length; i++) {
        let cookiePair = cookieArr[i].split("=");
        /* Removing whitespace at the beginning of the cookie name
        and compare it with the given string */
        if(name == cookiePair[0].trim()) {
            // Decode the cookie value and return
            return decodeURIComponent(cookiePair[1]);
        }
    }
    // Return null if not found
    return null;
}