//GTAG
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.defer=true;j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KQDCR4V');
//GANA
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.defer=true;a.async=true;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'G-6ZJ9NLF62L', 'auto');
    ga('set', 'anonymizeIp', true);
    ga('send', 'pageview');
//GTAG
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'G-6ZJ9NLF62L');
//emblue
(function(w,d,k,t,u,s,c,f){f=function(t){t=new Date();return t.getFullYear()+''+(t.getMonth()+1)+''+t.getDate()+'T'+t.getHours()+''+t.getMinutes()+''+t.getSeconds()};
u='https://widgets-api.embluemail.com/scripts/855ED939AFB4A42/85/'+f();w[k]=w[k] || [];s=d.createElement(t);
s.async=1;s.defer=true;s.src=u;c=d.getElementsByTagName(t)[0];c.parentNode.insertBefore(s,c);})(window,document,'_swdg','script');