const headSticky = function(){ window.scrollY >= 1 ? document.body.classList.add("change") : document.body.classList.remove("change") }
if( window.addEventListener("scroll",headSticky), document.body.classList.contains("body--home") ){}

if(document.body.classList.contains('nested-targets')) {
document.querySelector('.menu_befs .menu_principal').removeAttribute('uk-tab');
}

window.addEventListener("load",function(){
    const e=document.querySelectorAll("svg[data-url]"), t=e.length;
    for(let l=0;l<t;++l){
        let t=e[l].getAttribute("data-url");
        e[l].removeAttribute("data-url"), fetchSVG(t,e[l])}
});
const fetchSVG=async function(e,t){
    let l=await fetch(e),a=await l.text();
    let s=(new DOMParser).parseFromString(a,"image/svg+xml").getElementsByTagName("svg");
    if(s.length){
        const e=(s=s[0]).attributes,l=e.length;
        for(let a=0;a<l;++a)
            if(e[a].specified)
                if("class"===e[a].name){
                    const l=e[a].value.replace(/\s+/g," ").trim().split(" "),s=l.length;
                    for(let e=0;e<s;++e)t.classList.add(l[e])}
            else t.setAttribute(e[a].name,e[a].value);
        for(;s.childNodes.length;)t.appendChild(s.childNodes[0])
    }
};

async function iarrow(){
    let t=document.getElementsByClassName("iflecha");
    for(let e of t){
        let t=document.createElementNS("http://www.w3.org/2000/svg","svg"),
            w=document.createElementNS("http://www.w3.org/2000/svg","use");
        w.setAttributeNS("http://www.w3.org/1999/xlink","xlink:href","#arrow_right"),
        t.classList.add("svg_18"),
        t.appendChild(w),
        await e.appendChild(t)
}} iarrow();

async function imundo(){
    let t=document.getElementsByClassName("imundo");
    for(let e of t){
        let t=e.getAttribute("data-mundo"),
            s=document.createElementNS("http://www.w3.org/2000/svg","svg"),
            n=document.createElementNS("http://www.w3.org/2000/svg","use");
        e.classList.contains("ichat")?s.classList.add("svg"):e.classList.contains("ihipericon")?s.classList.add("svg_18"):s.classList.add("svg_64");
        n.setAttributeNS("http://www.w3.org/1999/xlink","xlink:href","#"+t),
        s.appendChild(n),
        await e.appendChild(s)
}} imundo();

async function ileft(){
    let t=document.getElementsByClassName("ileft");
    for(let e of t){
        let t=e.getAttribute("data-svg"),
            s=document.createElementNS("http://www.w3.org/2000/svg","svg"),
            n=document.createElementNS("http://www.w3.org/2000/svg","use");
        s.classList.add("svg_18");
        n.setAttributeNS("http://www.w3.org/1999/xlink","xlink:href","#"+t),
        s.appendChild(n),
        await e.prepend(s)
}} ileft();
async function iright(){
    let t=document.getElementsByClassName("iright");
    for(let e of t){
        let t=e.getAttribute("data-svg"),
            s=document.createElementNS("http://www.w3.org/2000/svg","svg"),
            n=document.createElementNS("http://www.w3.org/2000/svg","use");
        s.classList.add("svg_18");
        n.setAttributeNS("http://www.w3.org/1999/xlink","xlink:href","#"+t),
        s.appendChild(n),
        await e.prepend(s)
}} iright();

